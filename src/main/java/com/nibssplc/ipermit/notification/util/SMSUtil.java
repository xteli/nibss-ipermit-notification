/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nibssplc.ipermit.notification.util;

import com.nibss.bvn.sms.vas2nets.VAS2NETSService;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author xteli
 */
public class SMSUtil {

    public SMSUtil() {

    }

    public boolean sendSMS(String sender, String receipient, String body) {
        System.out.println(" ..: Inside sendSMS() :.. ");
        boolean sent = false;
        System.out.println(" ..: About formating receipient : " + receipient);
        receipient = "234" + receipient.substring(1);
        System.out.println(" ..: Formatted receipient : " + receipient);

        try {
            VAS2NETSService.execute(receipient, body, sender);
            sent = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println(" ..: Leaving sendSMS() :.. ");
        return sent;
    }

//    public static void main(String args[]) {
//        SMSNotification sms = new SMSNotification("opedisu@gmail.com", "08029050002", "http://login.betasms.com/customer/api/");
//        String to = "2347038025722";
//        String response = sms.sendSMS("Changi", to, "Dear+Customer+Welcome+to+Changi");
//        System.out.println(response);
//    }
}
