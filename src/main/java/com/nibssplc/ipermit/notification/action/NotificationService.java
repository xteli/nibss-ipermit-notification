/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nibssplc.ipermit.notification.action;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nibssplc.ipermit.lib.entity.Notification;
import com.nibssplc.ipermit.lib.util.Util;
import java.util.Date;
import java.util.List;
import com.nibssplc.ipermit.notification.util.SMSUtil;
import com.nibssplc.ipermit.lib.dao.IPermitSystem;
import com.nibssplc.ipermit.lib.entity.IPermitTokenRequest;
import com.nibssplc.ipermit.lib.entity.SystemAudit;
import com.nibssplc.ipermit.lib.util.Enum.NotificationStatus;
import com.nibssplc.ipermit.lib.util.Enum.NotificationType;
import com.nibssplc.ipermit.lib.util.Enum.OperationName;
import com.nibssplc.ipermit.lib.util.Enum.ServiceType;
import com.nibssplc.ipermit.lib.util.StatusCode;
import com.nibssplc.ipermit.notification.dto.NotificationResponse;

/**
 *
 * @author xteli
 */
public class NotificationService {

    IPermitSystem ipermitSystem = new IPermitSystem();
    NotificationResponse nResponse = new NotificationResponse();
    ObjectMapper objectMapper = new ObjectMapper();
    SystemAudit systemAudit = new SystemAudit();
    Util config = new Util();
    //sms parameters
    String apiUsername = "", apiPassword = "", baseSMSUrl = "";
    String registrationSMSTemplate = "", transactionSMSTemplate = "", smsBody = "", registrationNotificationSubject = "", transactionNotificationSubject = "";
    String smsNotificationStatus = "";
    String smtpServer = "", emailPassword = "", encryptionKey = "", apiType = "", apiRouting = "";
    int smtpPort = 0;
    boolean smsLiveMode = false, sendMail = false;

    public NotificationService() {
        try {
            smsLiveMode = Boolean.parseBoolean(config.getParameter("smsLiveMode"));
            sendMail = Boolean.parseBoolean(config.getParameter("sendMail"));
        } catch (NumberFormatException nfe) {
            sendMail = false;
            smsLiveMode = false;
        }
    }

    private void doTokenNotification() {
        String serviceResponse = "", emailResponse = "";
        System.out.println(" ..: Inside doCustomerNotification() :.. ");
        String status = "", message = "", otherInfo = "";
        boolean itemsFound = false;
        List<Notification> pendingNotifications = null;
        SMSUtil smsUtil = null;
        IPermitTokenRequest tokenRequest = null;
        boolean notificationSent = false;
        try {
            int count = 0;
            String receipient = "", messageBody = "", sender = "", subject = "";
            System.out.println(" ..: About fetching pending notification :.. ");
            pendingNotifications = ipermitSystem.retrievePendingNotifications();
            if (pendingNotifications != null) {
                if (!pendingNotifications.isEmpty()) {
                    itemsFound = true;
                    systemAudit.setOperationName(OperationName.Notification.name());
                    systemAudit.setOperationDate(new Date());
                    String clearReq = "{\"request\":\"notification\"}";
                    System.out.println("JSON Request (Clear) for Notification Service : " + clearReq);
                    systemAudit.setPlainRequest(clearReq);
                    System.out.println(pendingNotifications.size() + " pending notification(s) found :.. ");
                    for (Notification userToNotify : pendingNotifications) {
                        sender = userToNotify.getSender();
                        receipient = userToNotify.getReceipient();
                        messageBody = userToNotify.getBody();
                        if (userToNotify.getNotificationType() == NotificationType.SMS) {
                            smsUtil = new SMSUtil();
                            if (smsLiveMode) {
                                notificationSent = smsUtil.sendSMS(sender, receipient, messageBody);
                            } else {
                                notificationSent = true;
                            }
                            System.out.println("SMS Gateway Response : " + notificationSent);
                            if (!notificationSent) {
                                userToNotify.setOtherInfo("NOT SENT");
                                userToNotify.setNotificationStatus(NotificationStatus.Failed);
                            } else {
                                count++;
                                userToNotify.setOtherInfo("SENT");
                                userToNotify.setNotificationStatus(NotificationStatus.Notified);
                                userToNotify.setDateNotified(new Date());
                                if (userToNotify.getServiceType() == ServiceType.OTP) {
                                    tokenRequest = ipermitSystem.retrieveIPermitTokenRequestByReference(userToNotify.getUniqueRef());
                                    if (tokenRequest != null) {
                                        tokenRequest.setDelivered(true);
                                        tokenRequest.setDateDelivered(new Date());
                                        ipermitSystem.updateEntity(tokenRequest);
                                    }
                                }
                            }
                        } else {
                            subject = userToNotify.getSubject();
//                            emailUtil = new EmailUtil(smtpServer, smtpPort, sender, emailPassword, subject);
//                            if (sendMail) {
//                                emailResponse = emailUtil.sendEmail(receipient, messageBody);
//                            } else {
//                                emailResponse = "INACTIVE";
//                            }
                            System.out.println("Email Response : " + emailResponse);
                            emailResponse = emailResponse.trim();
                            userToNotify.setOtherInfo(emailResponse);
                            switch (emailResponse) {
                                case "SENT":
                                    count++;
                                    userToNotify.setNotificationStatus(NotificationStatus.Notified);
                                    userToNotify.setDateNotified(new Date());
                                    if (userToNotify.getServiceType() == ServiceType.OTP) {
                                        tokenRequest = ipermitSystem.retrieveIPermitTokenRequestByReference(userToNotify.getUniqueRef());
                                        if (tokenRequest != null) {
                                            tokenRequest.setDelivered(true);
                                            tokenRequest.setDateDelivered(new Date());
                                            ipermitSystem.updateEntity(tokenRequest);
                                        }
                                    }

                                    break;
                                default:
                                    userToNotify.setNotificationStatus(NotificationStatus.Failed);
                                    break;
                            }
                        }

                        ipermitSystem.updateEntity(userToNotify, "notification");
                    }
                    status = (count == pendingNotifications.size()) ? StatusCode.SUCCESSFUL : StatusCode.NOTIFICATION_EXCEPTION;
                    message = (count == pendingNotifications.size()) ? "Success" : "Partial Success";
                    otherInfo = count + " out of " + pendingNotifications.size() + " notifications sent.";
                } else {
                    System.out.println("No pending notification found :.. ");
                    status = StatusCode.NO_RECORD_FOUND;
                    message = "NO RECORD FOUND";
                }
            }
        } catch (Exception ex) {
            status = StatusCode.GENERAL_EXCEPTION;
            message = "Error in sending notification : " + ex.getMessage();
            System.err.println("Error occurred in doTokenNotification() : " + ex.getMessage());
        } finally {
            nResponse.setMessage(message);
            nResponse.setStatus(status);
            nResponse.setOtherInfo(otherInfo);
            try {
                serviceResponse = objectMapper.writeValueAsString(nResponse);
                System.out.println("Notification Response : " + serviceResponse);
                if (itemsFound) {
                    systemAudit.setPlainResponse(serviceResponse);
                    systemAudit.setIpAddress("");
                    systemAudit.setStatusCode(status);
                    systemAudit.setStatusMessage(message);
                    ipermitSystem.saveEntity(systemAudit);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println(" ..: Leaving doTokenNotification() :.. ");
    }

    public static void main(String[] args) throws Exception {
        NotificationService alertService = new NotificationService();
        System.out.println(" ..: iPermit Notification Service Started :.. ");
        int timeToWait = Integer.parseInt(args[0]);
        //  int timeToWait = 120;
        System.out.println(" ..: Notification is ongoing :.. ");
        while (!Thread.currentThread().isInterrupted()) {
//            alertService.gatewayNotificationUrl = args[1];
            alertService.doTokenNotification();
            try {
                System.out.println(" Service is waiting for " + timeToWait + " seconds ");
                Thread.sleep(timeToWait * 1000);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
